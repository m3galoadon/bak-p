"""
--------------------------------------------------------------------------
Bython - Python file manager utility
--------------------------------------------------------------------------
2022 - M3GALOADON
--------------------------------------------------------------------------
DEPENDENCIES:
    OS module
--------------------------------------------------------------------------
It can:
    - Read and copy files
    - Read and copy folders + subfolders + subfiles / recursively
    - Handle file as Bytes

It can't:
    - Create files from inexisting files
    - Copy only a certain content of a folder
    - Handle safe copying (it won't check if the file is in use and will copy it anyway)
    - Erase a file/folder that is in use
    - Cook a chocolate cake
--------------------------------------------------------------------------
"""

import os
# import time

Bython_VERSION = "1.0.0"

class ByteFile:
    path = ""

    # init
    def __init__(self,path):
        self.path = path

    # set object to new file
    def set(self,path):
        self.path = path

    # read bytes from file
    def read(self):
        with open(self.path, 'rb') as bytefile:
            content = bytefile.read()
            bytefile.close()
        return content

    # copy the file to another destination
    def copy(self, destination_path):
        with open(destination_path, 'wb') as dest:
            dest.write(self.read())
            dest.close()

    # delete the file
    def delete(self):
        os.remove(self.path)


class ByteFolder:
    path = ""

    #init
    def __init__(self,path):
        self.path = path

    # get the name of the current folder
    def get_name(self):
        name_path = self.path.split('/')
        return name_path[len(name_path) - 1]

    # list all folders within the folder
    def list_folders(self):
        folders = []
        for item in os.listdir(self.path):
            # os.chdir(os.path.abspath(self.path))
            item_path = os.path.join(self.path, item)
            if os.path.isdir(item_path):
                folders.append(item)
        return folders

    # list all files within the folder
    def list_files(self):
        files = []
        for item in os.listdir(self.path):
            item_path = os.path.join(self.path, item)
            if os.path.isfile(item_path):
                files.append(item)
        return files

    # return a tree dictionnary
    def recursive_navigation(self):
        tree_dict = {}
        # print("Looking into %s" % self.path)
        for dir in self.list_folders():
            directory = ByteFolder(os.path.join(self.path,dir))
            tree_dict[dir] = directory.recursive_navigation()

        for file in self.list_files():
            file_object = ByteFile(self.path + "/%s" % file)
            tree_dict[file] = "file"

        return tree_dict

    # copy the folder and content to the destination path
    def copy(self,destination_path):
        # Get the name
        # create the folder with the desired name
        os.makedirs(destination_path, exist_ok=True)
        for item in self.list_files():
            # inside it, create files for every file in the dict
            file = ByteFile(self.path + ("/%s" % item))
            file.copy(destination_path + ("/%s" % item))
        for item in self.list_folders():
            # loop through the folders
            folder = ByteFolder(self.path + ("/%s" % item))
            # recursively copy function from the path + folder name
            folder.copy(destination_path + ("/%s" % item))

    # delete folder and all it contains
    def delete(self):
        for item in self.list_files():
            file = ByteFile(self.path + "/" + item)
            file.delete()

        for item in self.list_folders():
            folder = ByteFolder(self.path + "/" + item)
            folder.delete()

        os.rmdir(self.path)


# Return the version of the programm
def bython_version():
    return Bython_VERSION


# return a formatted string of folder/files/subfolders/subfiles
def concatenate_dictionnary_string(dict,tabs):
    result = ""
    for item in dict.keys():
        if tabs == 0:
            result += ("  " * tabs) + item + "\n"
        else:
            result += ("  " * tabs) + ("> " * int(tabs / tabs)) + item + "\n"
        if not dict[item] == "file":
            result += concatenate_dictionnary_string(dict[item],tabs + 1)

    return result