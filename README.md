# BAK-P
Bak-p is a Backup utility for minecraft worlds.
It can back-up local minecraft worlds with different names. There is also a "quick save" and "quick load" function.
It can't save or load worlds in use, you must go back to the main menu before using it.
It is usefull when testing adventure/escape-games maps.
Bak-p is only intended for windows and has not been made for / tested on linux.

# Install (with pyinstaller)
from a terminal:
`pyinstaller bak-p.py --onefile`

# Usage
from a terminal :
`python bak-p.py`

or simply launch the compiled executable produced by PyInstaller