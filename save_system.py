"""
--------------------------------------------------------------------------
Fonctions de sauvegardes
Bak-P
--------------------------------------------------------------------------
DEPENDENCIES:
    - Bython
        > os
--------------------------------------------------------------------------
"""

from bython import *

class Save:
    def __init__(self,path_to_file, minecraft_folder):
        try:
            self.path = path_to_file
            self.backup_folder = "%s/Bak-P" % minecraft_folder
            self.save_folder = "%s/saves" % minecraft_folder
            self.source_file = ByteFolder(self.path)
            self.name = self.source_file.get_name()
        except Exception as err:
            print("[ CRITICAL ERROR ] - error occured while initiating save file")
            print(err)

    def get_name(self):
        return self.name

    def get_backup_name(self):
        return self.backup_name

    def open(self):
        local_path = "%s/.temp/%s/" % (self.backup_folder , self.source_file.get_name())
        try :
            self.source_file.copy(local_path)
            self.temp_file = ByteFolder(local_path)
            print("\t\tFile accessible.")
            return 0
        except Exception as err:
            print("\t\tFile not accessible. Closing.")
            print(err)
            self.temp_file = ByteFolder(local_path)
            self.temp_file.delete()
            return 1

    def close(self):
        try :
            self.temp_file.delete()
            print("\t\tTemp file deleted.")
            return 0
        except Exception as err:
            print("\t\tError while closing:")
            print(err)
            return 1

    def backup(self, save_name):
        try :
            local_path = "%s/%s/%s/" % (self.backup_folder, self.source_file.get_name(), save_name)
            self.temp_file.copy(local_path)
            print("\t\tSave ok!")
            return 0
        except Exception as err:
            print("[ CRITIAL ERROR ] - Error occured while copying files from temp folder!")
            print(err)
            print(local_path)
            return 1

    def load(self, world_name):
        try :
            local_path = "%s/%s/" % (self.save_folder, world_name )
            self.temp_file.copy(local_path)
            print("\t\tLoad ok!")
            # print(local_path)
            return 0
        except Exception as err:
            print("[ CRITIAL ERROR ] - Error occured while copying files from temp folder!")
            print(err)
            print(local_path)
            return 1
