"""
--------------------------------------------------------------------------
MUPy - Minecraft Usefull Python-lib
M3GALOADON - 2022
--------------------------------------------------------------------------
Made for Bak-P - Minecraft Map Backup Utility.
Windows only.
--------------------------------------------------------------------------
"""

#-----------------------DEPENDENCIES------------------------------------------------------------------------
import os
MUPy_VERSION = "0.1.0"

#-----------------------MAIN-DEFS---------------------------------------------------------------------------
def get_mupy_version():
    return MUPy_VERSION

def is_file_used(fpath):
    # deprecated, need to have a full file manager module (cutsom)
    try:
        # try to rename the folder to see if it is in use
        os.rename(fpath, fpath)
        return False
    except OSError as e:
        return True

#-----------------------MINECRAFT-DEFS----------------------------------------------------------------------
def get_minecraft_folder():
    userpath = os.path.expanduser('~')
    mc_folder = "%s/AppData/Roaming/.minecraft/" % userpath
    return mc_folder
