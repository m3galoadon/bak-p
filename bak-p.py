"""
--------------------------------------------------------------------------
Bak-P - Minecraft Map Backup Utility
M3GALOADON - 2022
--------------------------------------------------------------------------
"""
VERSION = "0.1.1"
quick = False
force_erase = False

#-----------------------IMPORTS-----------------------------------------------------------------------------
# tkinter base
from tkinter import *
# tkinter advanced needed
from tkinter import filedialog, messagebox
# os operations
import os
# custom module
from mupy import * # Minecraft Usefull Python lib
from bython import * # Bytes files and folders in python
from save_system import *

print(" [ + ] - Imports completed.")

#-----------------------NEW-FUNCTIONS-----------------------------------------------------------------------
# with save_system:

def save_map():
    global actual_save
    global backup_folder
    global savename
    global actual_savelist
    global true_list_frame
    global quick

    log("Saving map..."," [ + ] - Saving map...")

    # check if a save has been selected
    if(actual_save.get() == "..."):
        log("No savegame selected !", " [ ! ] An error occured while saving !\n\tNo savegame selected, please select one using the '...' button !")
        messagebox.showerror("No file !", "No backup selected.")
        return 1

    if check_if_save_exist() == 1:
        log("Aborting: User choose to not overwrite file.", " [ ! ] - Aborting: User choose to not overwrite file.")
        update_savelist()
        return 1

    # get the world name
    world_name = actual_save.get().split("/")
    world_name = world_name[len(world_name) - 1]

    # copy the file
    save_file = Save(actual_save.get() , get_minecraft_folder())

    if save_file.open() == 1:
        log("Aborting: File in use.", " [ ! ] - Aborting: File in use.")
        messagebox.showerror("File in use !", "Bak-P can't copy world that are in use.")
        update_savelist()
        return 1

    if quick:
        save_file.backup(".quick_save_bak-p_auto_gen")
        log("Quicksaving...", " [ + ] - Quicksaving...")
    else:
        save_file.backup(savename)
        log("Saving..."," [ + ] - Saving...")

    if save_file.close() == 1:
        log("Critical error occured." , " [ ! ] - Critical error occured while closing .temp file.")
        messagebox.showerror("Critical error !", "A critical error occured.")
        update_savelist()
        return 1

    log("Map saved.", " [ + ] - Map saved.")
    update_savelist()
    return 0


def load_map():
    global actual_save
    global backup_folder
    global actual_savelist
    global true_list_frame
    global quick

    log("Loading map...", " [ + ] - Loading map...")

    # check if a save has been selected
    if(actual_save.get() == "..."):
        log("Aborting: No map selected.", " [ ! ] - Aborting: No map selected.")
        messagebox.showerror("No file !", "No backup selected.")
        return 1;

    # get the world name
    world_name = actual_save.get().split("/")
    world_name = world_name[len(world_name) - 1]

    # copy the file
    if quick :
        selected_save = ".quick_save_bak-p_auto_gen"
        log("Loading quicksave..." , " [ + ] - Loading quicksave...")
    else:
        selected_save = true_list_frame.get(ACTIVE)
        log("Loading %s..." % selected_save , " [ + ] - Loading %s..." % selected_save)

    save_file = Save("%s/%s/%s" % (backup_folder, world_name, selected_save), get_minecraft_folder())

    if save_file.open() == 1:
        log("Aborting: File in use.", " [ ! ] - Aborting: File in use.")
        messagebox.showerror("File in use !", "Bak-P copy world that are in use.")
        update_savelist()
        return 1

    if save_file.load(world_name) == 1:
        log("Aborting: File in use.", " [ ! ] - Aborting: File in use.")
        messagebox.showerror("File in use !", "Bak-P copy world that are in use.")
        return 1

    if save_file.close() == 1:
        log("A critical error occured.", " [ ! ] - Critical error while closing .temp file !")
        messagebox.showerror("Critical error !", "A critical error occured.")
        update_savelist()
        return 1

    log("Map loaded.", " [ + ] - Map loaded.")
    update_savelist()
    return 0


#-----------------------FUNCTIONS---------------------------------------------------------------------------
def select_map():
    global actual_save
    global backup_folder

    log("Selecting map..." , " [ + ] - Selecting map...")

    # ask for a directory and store it to our StringVar so it can show on the button
    root.world_directory = filedialog.askdirectory(initialdir = save_folder, title = "World to backup")
    actual_save.set(root.world_directory)
    log("Selected: %s." % actual_save.get() , "\tSelected Backup:\n\t\t%s" % actual_save.get())
    update_savelist()


def update_savelist():
    global actual_save
    global backup_folder
    global found_list
    global actual_savelist

    # get the world name
    world_name = actual_save.get().split("/")
    world_name = world_name[len(world_name) - 1]

    path = (backup_folder + world_name + '/')
    log(log_out.get(), " [ + ] - Looking for existing saves in:\n\t%s" % path)

    try:
        found_list = os.listdir(path.replace("\\","/"))
        real_list = []
        log(log_out.get() , "\tFound " + str(len(found_list)) + " saves:")
        for s in found_list:
            log(log_out.get() , "\t\t- " + str(s))
            # verify if save is correct and delete it if it is not
            #                                      ~~~~~~~~~~~ wow that's a sentence
            if os.path.exists(path + s + "/data/"):
                if str(s) != ".quick_save_bak-p_auto_gen":
                    real_list.append(s)
            else:
                log(log_out.get()  , "\tFound an uncomplete world !\n\t\tDeleting..", end = '')
                to_del = ByteFolder(path + s)
                to_del.delete()
                log(log_out.get() , "\t\tDeleted.  ")

        actual_savelist.set(value = real_list)

    except Exception as err:
        log(log_out.get() , "\tNo saves found.")
        actual_savelist.set(value = [])

    except OSError as oserr:
        log(log_out.get(), " [ ! ] - An OSerror occured while saving !")
        log(log_out.get() , "\t%s" % oserr)


def check_if_save_exist():
    global savename
    global backup_folder

    # get the world name
    world_name = actual_save.get().split("/")
    world_name = world_name[len(world_name) - 1]

    log("Checking if file exists.." , " [ + ] - Checking if file exists...")

    if(os.system("cd " + '"' + backup_folder + "/" + world_name + "/" + savename + '"') == 0):
        log("Overwrite the file ?" , "\nBackup already exist !\n\tOverwrite it ?")
        overwrite = messagebox.askyesno("WARNING!", "This backup already exists !\nOverwrite it ?")
        if overwrite:
            log(log_out.get() , "\tYes.")
            return False
        else:
            log(log_out.get(), "\tNo.")
            return True
    else:
        log(log_out.get(), "\tFile does not exist.")
        return False


def quick_save():
        global quick

        quick = True
        save_map()
        quick = False


def quick_load():
    global quick

    quick = True
    load_map()
    quick = False


def change_name(ignored):
    # Ignoring the argument given by the entry as we don't f*cking care
    global savename
    global save_name_input

    savename = save_name_input.get()
    savename = save_name_input.get()
    log("Name: \"%s\"" % savename , " [ + ] - Savename changed: \"%s\"" % savename)


def delete_backup():
    global savename
    global actual_save
    global backup_folder
    global true_list_frame

    # get the world name
    world_name = actual_save.get().split("/")
    world_name = world_name[len(world_name) - 1]

    selected_save = true_list_frame.get(ACTIVE)
    log("Deleting %s..." % selected_save, " [ + ] - Deleting %s..." % selected_save)

    try:
        save = ByteFolder("%s/%s/%s" % (backup_folder, world_name, selected_save))
        log("Are you sure ?", "\t Are you sure ?")
        # log_out.set("Are you sure ?")
        delete = messagebox.askyesno("Are you sure ?", "You are about to permanently delete \"%s\" backup. Are you sure ?" % selected_save )
        if delete:
            # log_out.set("Aborting.")
            log("Deleting...", "\tYes.")
            save.delete()
            log("File deleted.", "\tFile deleted.")
            update_savelist()
        else:
            log("Aborting.", "\tNo.")
        return 0
    except Exception as err:
        log("A critical error occured." , " [ ! ] - A critical error occured while deleting a backup:\n%s" % err)
        messagebox.showerror("Critical error !", "A critical error occured.")
        update_savelist()
        return 1


def log(soft_message, hard_message):
    global root
    print(hard_message)
    log_out.set(soft_message)
    root.update()

#-----------------------INIT--------------------------------------------------------------------------------


save_folder = get_minecraft_folder() + "saves/"
backup_folder = get_minecraft_folder() + "Bak-P/"
savename = "My backup"

root = Tk()
root.title("Bak-P " + VERSION)
root.geometry("812x312")
root.resizable(False,False)
actual_save = StringVar()
actual_save.set("...")
actual_savelist = StringVar()
actual_savelist.set(value = [])
log_out = StringVar()
log_out.set("Starting program...")


# Base frame
top_frame = Frame(root)
# Top frames
left_frame = Frame(top_frame, height = 400)
right_frame = Frame(top_frame, height = 400)

# Map select
map_select_label = Label(left_frame, text = "World location: ", width = 50, anchor = "nw")
map_select_label.pack(side = TOP)

map_select_button = Button(left_frame, textvariable  = actual_save, command = select_map, width = 50)
map_select_button.pack(side = TOP)

# Save name
save_name_label = Label(left_frame, text = "Backup name:", width = 50, anchor = "nw")
save_name_label.pack(side = TOP)

save_name_input = Entry(left_frame, width = 50)
save_name_input.pack(side = TOP)
save_name_input.insert(0, "My Backup")
save_name_input.bind("<KeyRelease>", change_name)


# Save list
# create main save list frame
save_list_frame = Frame(right_frame, height = "50px", width = "320px")
save_list_frame.propagate(False)

# Explanation label
save_list_label = Label(right_frame, text = "Backups:", width = 50, anchor = "nw")
save_list_label.pack(side = TOP, expand = NO, fill = X)

# create the scrollbar
save_list_scroll_bar = Scrollbar(save_list_frame, orient = VERTICAL)

# create the list frame
true_list_frame = Listbox(save_list_frame, listvariable = actual_savelist ,yscrollcommand = save_list_scroll_bar.set)

# pack the scrollbar
save_list_scroll_bar.pack(side = RIGHT, fill = BOTH)
save_list_scroll_bar.config( command = true_list_frame.yview )

# pack the list
true_list_frame.pack(side = LEFT , fill = BOTH, expand = YES)

# pack the main save list frame
save_list_frame.pack(side = TOP, fill = BOTH, expand = YES)


# Pack main frames
left_frame.pack(side = LEFT, fill = BOTH, expand = YES)
right_frame.pack(side = RIGHT, fill = BOTH, expand = YES)
top_frame.pack(side = TOP, fill = BOTH, expand = YES)



# Middle frames
middle_frame = Frame(root)
save_frame = Frame(middle_frame)
load_frame = Frame(middle_frame)

# Save button
save_button = Button(save_frame, text = "Save", command = save_map, width = 50)
save_button.pack(side = TOP)

# Load button
load_inner_frame = Frame(load_frame)
load_button = Button(load_inner_frame, text = "Load", command = load_map, width = 25)
load_button.pack(side = LEFT)

# delete
delete_button = Button(load_inner_frame, text = "Delete", command = delete_backup, width = 23)
delete_button.pack(side = RIGHT, expand = True)

load_inner_frame.pack(side = TOP)

# Quick options
quick_save_button = Button(save_frame, text = "Quick save", command = quick_save, width = 50)
quick_save_button.pack(side = TOP)

quick_load_button = Button(load_frame, text = "Quick load", command = quick_load, width = 50)
quick_load_button.pack(side = TOP)

# Middle frames pack
save_frame.pack(side=LEFT, fill = Y)
load_frame.pack(side=RIGHT, fill = Y)
middle_frame.pack(side = TOP , fill = Y)

# Log output frame
log_frame = Frame(root)

# Textbox // replace with a real textbox
log_text = Label(log_frame, textvariable  = log_out, width = 50, anchor = NW, justify = LEFT)
log_text.pack(side = TOP , fill = X)

# Pack log frame
log_frame.pack(side = TOP, fill = BOTH)


#-----------------------MAINLOOP----------------------------------------------------------------------------
log_out.set("Welcome")
root.mainloop()

print("[+] Program ended.")
